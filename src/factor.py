# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from __future__ import annotations  # requires python 3.7+ (needed for type hints)
import pandas as pd

class Factor():
    """
    Class that stores a table of 'probabilities' for 1 or more
    variables and provides some useful methods to manipulate Factors. 
    
    Attributes:
        probabilities: a pandas DataFrame containing the 
                probabilities 
    """

    def __init__(self, probs: pd.DataFrame) -> None:
        assert type(probs) == pd.DataFrame, "Factor must be initialized with a pandas.DataFrame."
        self.probabilities = probs.copy()


    def reduce(self, observed: dict) -> None:
        """ Reduce the Factor to the OBSERVED variables. """

        cols = self.probabilities.columns.drop('prob')
        for var, value in observed.items():
            if var in cols:
                self.probabilities = self.probabilities[self.probabilities[var] == value]


    def normalize(self):
        """ Normalized self.PROBABILITIES to make the entries
        probabilities. """

        sum_of_probs = self.probabilities['prob'].sum()
        self.probabilities['prob'] = self.probabilities['prob'].div(sum_of_probs)


    def multiply(self, other: Factor) -> Factor:
        """ Multiply two Factors and return the new Factor. """

        assert type(other) == Factor, "Can only multiply two Factors."

        prob_other = 'prob_other'  # temporary column name

        # the rename of the 'prob' column require a copy of the whole
        # table to prevent a SettingWithCopyWarning from pandas
        other_probs = other.probabilities.rename(columns={'prob': prob_other})
        # pd.merge merges the two factor into one table based on the
        # columns that they have in common.
        merged = pd.merge(left=self.probabilities, right=other_probs, how='inner')
        # to get the multiplication of the two Factors simply
        # multiply the two probability column of the merged
        # DataFrames.
        merged.prob = merged.prob * merged.prob_other
        merged.pop(prob_other)

        return Factor(merged)


    def sum_out_var(self, var: str=None) -> None:
        """ If VAR is not given then sum up all rows with equal truth
        values. If VAR is given then sum out the variable VAR from
        the probability table. """

        new_probabilities = pd.DataFrame()

        if var:
            self.probabilities.pop(var)
        truth_cols = self.probabilities.columns.drop('prob')

        while not self.probabilities.empty:
            truth_values = self.probabilities[truth_cols]
            first_row_of_truth_values = truth_values.iloc[0].copy()

            # booleant mask indicating which rows of PROBABILITIES
            # has the same truth values as the first row.
            has_truth_values_of_first_row = (truth_values == first_row_of_truth_values).all(axis=1)
            # select all rows with all the same truth values
            selected_rows = self.probabilities[has_truth_values_of_first_row]

            # sum up the probabilities of the selected rows
            sum_of_probs = selected_rows['prob'].sum()

            # add the summed probabilities with the truth values to
            # the new probabilities DataFrame
            first_row_of_truth_values.loc['prob'] = sum_of_probs
            new_probabilities = pd.concat([new_probabilities, first_row_of_truth_values.to_frame().transpose()], ignore_index=True)

            # remove all rows from PROBABILITIES that have their sums
            # already computed
            self.probabilities = self.probabilities[has_truth_values_of_first_row == False]
        
        # use the DataFrame without VAR as the new probabilities DataFrame
        self.probabilities = new_probabilities


    def get_variables(self) -> pd.Index:
        """ Return a pandas.Index with all variable names of the
        Factor. """

        return self.probabilities.columns.drop('prob')

    
    def to_string(self) -> str:
        """ Return a string representing the Factor. """

        s = 'f('
        for v in self.get_variables():
            s += v + ', '
        return s[:-2] + ')' if len(s) > 2 else 'f()'
