# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from heuristics.heuristic import Heuristic
from read_bayesnet import BayesNet

class Least_Incoming_Arcs_First_Heuristic(Heuristic):
    """ Heuristic that determines the order of elimination based on
    the number of incoming arcs of a node in the network. """

    def __init__(self):
        pass


    def get_elimination_order(self, network: BayesNet, query: str) -> list:
        """ The order is the least incoming arcs first (i.e. nodes
        with the fewest parents first). """

        # counts: contains the number of parents per node
        counts = list(map(lambda kv: (kv[0], len(kv[1])), network.parents.items()))
        # sort the node counts on the counts
        sorted_counts = sorted(counts, key=lambda kv: (kv[1], kv[0]))
        # keep the node names and remove the counts (keeping the order)
        sorted_nodes = list(map(lambda kv: kv[0], sorted_counts))
        sorted_nodes.remove(query)

        return sorted_nodes


    def to_string(self) -> str:
        """ Return the string identifying the heuristic. """

        return "Least_Incoming_Arcs_First"
