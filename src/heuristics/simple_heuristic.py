# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from heuristics.heuristic import Heuristic
from read_bayesnet import BayesNet

class Simple_Heuristic(Heuristic):
    """ Class that computes the eliminiation order in a very
    simplistic way. """

    def __init__(self):
        pass


    def get_elimination_order(self, network: BayesNet, query: str) -> list:
        """ The order is the order from which they are read from the
        .bif file (i.e. as they are found in network.nodes). """

        order = network.nodes.copy()
        order.remove(query)
        return order
                

    def to_string(self) -> str:
        """ Return the string identifying the heuristic. """

        return "Simple"
