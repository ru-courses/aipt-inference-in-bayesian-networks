# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from abc import ABC, abstractmethod

from read_bayesnet import BayesNet

class Heuristic(ABC):
    """ Abstract class with methods that must be implemented by
    heuristics. """

    @abstractmethod
    def get_elimination_order(network: BayesNet, query: str) -> list:
        """ Return a list with the order in which to eliminate the
        variables of the NETWORK, excluding the QUERY variable. """

        pass


    @abstractmethod
    def to_string() -> str:
        """ Return the string identifying the heuristic. """

        pass
