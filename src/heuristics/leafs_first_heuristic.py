# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from heuristics.heuristic import Heuristic
from read_bayesnet import BayesNet

class Leafs_First_Heuristic(Heuristic):
    """ Heuristic that determines the order of elimination on whether
    a node is a leaf or not. """

    def __init__(self):
        pass


    def get_elimination_order(self, network: BayesNet, query: str) -> list:
        """ The order is leaf nodes before the rest. """

        leafs = network.nodes.copy()
        leafs.remove(query)
        non_leafs = []
        for parents in network.parents.values():
            if not leafs:  # there are not leafs in the network
                break
            for parent in parents:
                try:
                    # if it is a parent it cannot be a leaf
                    leafs.remove(parent)
                    non_leafs.append(parent)
                except ValueError:
                    pass

        return leafs + non_leafs
                

    def to_string(self) -> str:
        """ Return the string identifying the heuristic. """

        return "Leafs_First"
