# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

from io import TextIOWrapper
import pandas as pd
import random

from factor import Factor
from heuristics.heuristic import Heuristic
from read_bayesnet import BayesNet

class VariableElimination():
    """ Class implementing useful methods for executing a Variable
    Elimination algorithms on a Baysian Network. """

    def __init__(self, network: BayesNet, open_log_file: TextIOWrapper):
        """
        Initialize the variable elimination algorithm with the specified network.
        Add more initializations if necessary.
        """

        self.network = network
        self.open_log_file = open_log_file
        self.multiplication_count = 0
        self.max_factor_size = 0


    def get_elimination_order(self, query: str, heuristic: Heuristic) -> list:
        """ Return the elimination order determined by the heuristic. """

        return heuristic.get_elimination_order(self.network, query)


    def get_query_variable(self) -> str:
        """ Return the query variable for VE. """

        nodes = self.network.nodes
        # Use a deterministic seed for reproducibility of the
        # results. The seed is the ascii value of the first char of
        # the firstnode
        random.seed(ord(nodes[0][0]))
        return nodes[random.randint(0, len(nodes)-1)]


    def get_observations(self, network: BayesNet, query: str, allow_observations: bool) -> dict:
        """ Return a dictionary of observation of the NETWORK. For
        testing purposes use the RANDOM module to generate the
        dictionary. """

        observations = {}

        if not allow_observations:
            return observations

        vars = network.nodes.copy()
        vars.remove(query)
        # Use a deterministic seed for reproducibility of the
        # results. The seed is the ascii value of the first letter of
        # QUERY.
        random.seed(ord(query[0]))
        while True:
            r = random.randint(0, len(vars)-1)  # NOTE: randint retuns values inclusive bounds
            var = vars[r]
            values = network.values[var]
            value = values[random.randint(0, len(values)-1)]
            observations[var] = value
            vars.remove(var)
            if random.randint(0,99) < 90 or not vars:
                break  # 10% change of adding an observation

        return observations


    def run(self, query: str, observed: dict, elim_order: list) -> pd.DataFrame:
        """
        Use the variable elimination algorithm to find out the probability
        distribution of the query variable given the observed variables

        Input:
            query:      The query variable
            observed:   A dictionary of the observed variables {variable: value}
            elim_order: Either a list specifying the elimination ordering
                        or a function that will determine an elimination ordering
                        given the network during the run

        Output: A variable holding the probability distribution
                for the query variable
        """

        # Identify factors and reduce observed variables
        factors = []
        for v, p in self.network.probabilities.items():
            factors.append(Factor(p))
            factors[-1].reduce(observed)

        # For every variable Z in elim_order:
        for var in elim_order:

            self.__print_factors(factors, 'All remaining factors:')
            self.open_log_file.write(f'Eliminate {var}\n')

            remaining_factors, \
            factors_to_multiply = self.__split_factors(var, factors)

            if factors_to_multiply:
                new_factor = factors_to_multiply[0]
                if len(factors_to_multiply) > 1:
                    # Multiply factors containing Z
                    new_factor = self.__multiply_factors(factors_to_multiply)
                if new_factor.probabilities.columns.size > 2:
                    """ if the number of columns of a Factors is 2 then
                    its columns must be 'VAR' and 'prob', therefore
                    after summing out VAR it leaves NEW_FACTOR empty
                    and hence these Factors can be remove from the
                    computation.
                    This can happend when a graph is not connected
                    (such as doc/sachs.big). """
                    
                    # sum out Z to optain f_z
                    self.open_log_file.write(f'\tSumming out {var} from {new_factor.to_string()} = ')
                    new_factor.sum_out_var(var)
                    self.open_log_file.write(f'{new_factor.to_string()}\n')

                    # Add f_z to remaining factors
                    remaining_factors.append(new_factor)
                factors = remaining_factors

        # return the normalized result.
        res = self.__multiply_factors(factors)
        res.sum_out_var()
        res.normalize()
        return res.probabilities


    def __print_factors(self, factors: Factor, mesg: str) -> None:
        """ Log the MESG and FACTORS to the OPEN_LOG_FILE. """

        self.open_log_file.write(f'{mesg}\n\t')
        for f in factors:
            self.open_log_file.write(f'{f.to_string()}, ')
        self.open_log_file.write('\n')


    def __split_factors(self, var: str, factors: list) -> tuple:
        """ Helper function.
        Split FACTORS into a list of Factors that contain the
        variable VAR and a list that does not. """

        remaining_factors = []
        factors_to_multiply = []

        for f in factors:
            if var in f.get_variables():
                factors_to_multiply.append(f)
            else:
                remaining_factors.append(f)

        return remaining_factors, factors_to_multiply


    def __multiply_factors(self, factors: list) -> Factor:
        """ Helper function.
        Multiply the list of FACTORS and return the resulting Factor.
        Keep track of the multiplication count and the max Factor
        size. """

        assert factors, "Cannot multiply empty list of Factors."

        self.__print_factors(factors, 'List of all factors to multiply:')

        f = factors[0]

        for o in factors[1:]:
            self.open_log_file.write(f'\tMultiplying: {f.to_string()} x {o.to_string()} = ')
            f = f.multiply(o)
            self.multiplication_count += 1
            self.open_log_file.write(f'{f.to_string()}\n')

        self.max_factor_size = max(self.max_factor_size, f.probabilities.size)

        return f
