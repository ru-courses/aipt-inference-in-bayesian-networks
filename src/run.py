# COURSE: AI Principles & Techniques
# AUTHOR: Bryan Rinders, s1060340
# RADBOUD UNIVERSITY
# ASSIGNMENT: Inference In Bayesian Networks
# DATE: 2022-12-21

import os
import sys

from heuristics.leafs_first_heuristic import Leafs_First_Heuristic
from heuristics.least_incoming_arcs_first_heuristic import Least_Incoming_Arcs_First_Heuristic
from heuristics.simple_heuristic import Simple_Heuristic
from read_bayesnet import BayesNet
from variable_elim import VariableElimination


def main():
    """ Run the Variable Elimination algorithm on the files in ../doc
    with several heuristics and log the process and results in
    appropriately named files in ../log. """

    # heuristics used to determine the elimination order of the variables
    heuristics = [Simple_Heuristic(),
                  Leafs_First_Heuristic(),
                  Least_Incoming_Arcs_First_Heuristic()]
    cwd = os.getcwd()  # current working directory

    # to prevent errors when reading and writing to files
    if cwd[-3:] != "src":
        print("Run `python main.py` from the src directory (sudoku-py/src/)", file=sys.stderr)
        sys.exit(1)

    docdir = os.path.join(cwd[:-3], "doc")  # path to .bif files
    logdir = os.path.join(cwd[:-3], "log")  # path to .log files
    os.makedirs(logdir, exist_ok=True)  # create log directory if necessary

    # test the variable elimination algorithm on multiple networks
    # with different heuristics and (not) using observed variables.
    for file in os.listdir(docdir):
        if file[-3:] == "bif":  # make sure to only use .bif files
            network = BayesNet(os.path.join(docdir, file))
            # query = network.nodes[-3]  # pick some node as query variable

            for heuristic in heuristics:
                # set up the logging file
                log_file = os.path.join(logdir, file[:-4] + f"-{heuristic.to_string()}.log")
                open_log_file = open(log_file, 'w+')

                for allow_observations in [False, True]:
                    ve = VariableElimination(network, open_log_file)
                    query = ve.get_query_variable()
                    observed = ve.get_observations(network, query, allow_observations)
                    elim_order = ve.get_elimination_order(query, heuristic)
                    # log QUERY and ELIM_ORDER
                    open_log_file.write(f'Query variable: {query}\n')
                    open_log_file.write(f'Elimination order: {elim_order}\n')
                    # run the VE algorithm
                    probabilities_of_query = ve.run(query, observed, elim_order)

                    # print the result to the console
                    print(f'File: {file}\n'
                          f'Heuristic: {heuristic.to_string()}\n',
                          f'Observed variables: {observed}\n'
                          f'Multiplication count: {ve.multiplication_count}\n',
                          f'Max Factor size: {ve.max_factor_size}\n',
                          f'Probabilities of {query}:\n{probabilities_of_query}\n')

                    # log the results of VE
                    open_log_file.write('\n' + \
                        f'Number of multiplications performed: {ve.multiplication_count}\n' + \
                        f'Largest Factors size encountered: {ve.max_factor_size}\n' + \
                        f'\nProbability distribution for the query variable, {query}' + \
                        f',\ngiven observed variables {observed}:\n' + \
                        f'{probabilities_of_query}\n\n' + 100 * '#' + '\n\n')

                open_log_file.close()


if __name__ == '__main__':
    main()
