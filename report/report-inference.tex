\documentclass[a4paper]{article}

\usepackage[backend=biber, style=apa]{biblatex}
\addbibresource{ref.bib}
\usepackage{minted}             % environment for adding code blocks
\usepackage{booktabs}           % nicer tables
\usepackage{multirow}

%\usepackage{tikz}              % environment for drawing things
\usepackage{float}              % for the 'H' positioning of figures
\usepackage{a4wide}             % To decrease the margins and allow more text on a page.
\usepackage{graphicx}           % To deal with including pictures.
\usepackage{color}              % To add color.
\usepackage{enumerate}          % To provide a little bit more functionality than with 
                                % LaTeX's default enumerate environment.
\usepackage{enumitem}
\usepackage{array}              % To provide a little bit more functionality than with 
                                % LaTeX's default array environment.
\usepackage{float}              % for the 'H' positioning of figures
\usepackage{listings}           % for the formatting of code blocks
\usepackage[american]{babel}
\usepackage{amsmath,amssymb}    % better equations

\lstset{                        % for the boxing of code blocks
    basicstyle=\footnotesize, 
    frame=single,
    breaklines=true,
    numbers=left,
    tabsize=2,
}

\newcommand{\CO}{\ensuremath{\mathcal{O}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{AI Principles \& Techniques\\Assignment 3: Inference in Bayesian Networks}

\author{Bryan Rinders, s1060340\\Radboud University}

\begin{document}
% \maketitle 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      The header                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}
\scshape 
	\large{\textbf{AI PRINCIPLES \& TECHNIQUES}}\\[5mm]
	\large{Assignment 3: Inference in Bayesian Networks}\\[5mm]
	\normalsize{Bryan Rinders \hfill \textit{s1060340}}\\
	\normalsize{Radboud University \hfill \textit{2022-12-21}}	\rule{\textwidth}{0.4pt} \\
	\vspace{0.3\baselineskip}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   The text contents                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Introduction}
% Sketch the context of the work so that the reader understand what
% they are reading.

There are many real world problems that can be represented as Bayesian
networks, for example disease diagnosis, spam filter and semantic
search. To efficiently computed the probability distribution of a node
in the network the Variable Elimination algorithm (VE) was
developed. VE uses Bayesian probability and some other clever tricks
to improve the efficiency over the obvious $\CO(2^n)$ algorithm
(i.e. compute the probability distribution of the entire network and
sum out all the unnecessary variables). This paper will show how to
use inference in Bayesian networks to infer the probability
distribution of a node in the network. It will do this by implementing
the Variable Elimination algorithm. Further more will we infestigate
the impact of several heuristics on the efficiency of VE.


\section{Method}

\subsection{Specification}
% Specify the task at hand, with the assignment text as your
% guideline. Describe what the software should do.

Given a Baysian network and a set of (conditional) probability
distribution for each node in the network and a set of observations in
the network, the goal is to produce the probability distribution for a
specified node in the network. It should also log the process,
i.e. record the query and observed variables, the elimination order
and the factors and which the factors are processed.


\subsection{Design}
% Specify here your software design. Specify the different classes and
% data structures you use, packages used for I/O and visualisation
% etc., and the pseudo code for existing algorithms that you will
% implement.

The programming language used to implement the VE algorithm is
Python. While Python is not the fastest language it has some useful
packages that greatly reduce the code complexity of certain aspects of
the algorithm, most notably the multiplication of factors.

\subsubsection{Packages}
The packages used to write this program are:

\begin{itemize}[noitemsep]
\item \_\_future\_\_ (for type hints)
\item abc (for creating abstract classes)
\item io (for type hints)
\item os (for getting the proper file paths on different OS'es)
\item pandas (mainly for its data frames)
\item random (for pseudo random functions)
\item sys (for writing to stderr and exiting with an error)
\end{itemize}

\subsubsection{Data Structures}
The most important data structure that is used is the pandas
DataFrame. It resembles a spread sheet and it makes serveral
complicated task (e.g multiplying factors and summing out variables)
very easy to implement.

\subsubsection{Classes}
The following classes have been implemented along with their most
notable methods.
\newline
\vspace*{.3 cm}
\newline
\begin{tabular}{l l}
  \multicolumn{2}{l}{\textbf{BayesNet}} \\
  Methods: & Notes: \\
  parse\_* & Several parse function that together construct a Bayesian
             network from a .bif file. \\
\end{tabular}
\newline
\vspace*{.2 cm}
\newline
The BayesNet class represents a Bayesian network and holds all the
important information on the network (e.g. node names and the domain
values and probability distrubutions per node). This class uses the
pandas.DataFrame data structure to store the probability
distributions.
\newline
\vspace*{.3 cm}
\newline
\begin{tabular}{l l}
  \multicolumn{2}{l}{\textbf{Factor}} \\
  Methods: 		& Notes: \\
  reduce 		& reduce the factor by the observed variables. \\
  normalize 	& normalize the factor to create a probability distribution. \\
  multiply 		& multiply two factors. \\
  sum\_out\_var & sum out a variable from a factor. \\
\end{tabular}
\newline
\vspace*{.2 cm}
\newline
The class Factor represent a factor. Factors are used by the VE
algorithm in order to find the desired probability distribution for a
node.
\newline
\vspace*{.3 cm}
\newline
\begin{tabular}{l l}
  \multicolumn{2}{l}{\textbf{VariableEliminaion}} \\
  Methods: 					& Notes: \\
  get\_elimination\_order 	& use a heuristic to find the elimination order. \\
  get\_query\_variable 		& randomly pick a query variable (for automated testing). \\
  get\_observations 		& randomly pick some observed variables (for automated testing). \\
  run 						& to run the VE algorithm on a Bayesian network. \\
\end{tabular}
\newline
\vspace*{.2 cm}
\newline
The VariableElimination class implements the VE algorithm plus some
useful methods for automated test of the heuristics and logging all
the steps taken by the algorithm.
\newline
\vspace*{.3 cm}
\newline
\begin{tabular}{l l}
  \multicolumn{2}{l}{\textbf{Heuristic}} \\
  Methods: 					& Notes: \\
  get\_elimination\_order 	& return the elimination order to be used by VE. \\
\end{tabular}
\newline
\vspace*{.2 cm}
\newline
Heuristic is an abstract class that every heuristic must implement in
order to be used by the VE algorithm. The heuristics that have been
implemented are:

\begin{enumerate}[noitemsep]
\item Simple (eliminate in the order in which the nodes have been read
  from the .bif file).
\item Leafs\_First (leaf nodes will be eliminated first).
\item Least\_Incoming\_Arcs (nodes with the fewest parents are
  eliminated first).
\end{enumerate}

\subsubsection{Pseudo Code}
Listing \ref{lst:VEpseudo} shows the pseudo code for Variable
Elimination.

\begin{listing}[H]
  \lstinputlisting[language=python]{ve.pseudo}
  \caption{Pseudo code for the Variable Elimination algorithm.}
  \label{lst:VEpseudo}
\end{listing}




\subsection{Implementation}
% Give (only) the crucial part of the implementation here, preferably
% with line numbers, and explain what it does without literally
% repeating the code. Highlight important aspects and don’t repeat
% trivial translations from the pseudo-code.
% Also give the complexity and correctness analyses of the algorithms.

\subsubsection{Variable Elimination Analysis}
Note: in this section all references to line numbers refer to listing
\ref{lst:VE} unless otherwise specified.

Listing \ref{lst:VE} is a condensed version of the actual
implementation of VE algorithm. It is condensed because the actual
implementation includes some logging and other things that do not
contribute to the workings and end result of the algorithm. Those
parts have therefore been removed.

The algorithm works by first finding all the factors. For simplicity
the factors are directly taken from the probability tables read from
the source files (from the example problems I have seen this does not
result in any problems). Then the variables are eliminated one by one
according to the elimination order until only the query variable is
left. Then if necessary it will multiply all remaining factors (which
only contain the query variable) and normalize the values to produce
the final probability table. A minor addition to the algorithm is line
18. This line allows the algorithm to properly run on disconnected
graphs / Bayesian networks.

\textbf{Complexity}: Let $n$ be the number of nodes and $d$ the
maximum domain size of the nodes in the network. Then the
initialization of the algorithm, lines, 4-6 is $\CO(n^2)$ because it
loops over each node in the network and the reduction of the factors,
line 6, can be at most by $n$ variables. The body of the algorithm,
lines 9-24, has $\CO(nd^n)$. The for loop of line 9 executes $n$ times
for every variable in the network and line 11 executes at most $n$
times to split the factors into those to multiply and the other
factors, leading to $\CO(n^2)$. The maximum size of a factor is $d^n$
and assuming that the complexity of pandas \verb+merge+ is $\CO(n^2)$
(I could not find any source for the actual complexity) then
multiplying two factors is $\CO(d^n)$. Summing out the variable, line
20, is $\CO(d^n)$, it will add every line in the data frame with equal
variable values. Lines 23-24 are constant and lines 27-28 multiply
factors and sum a variable which as explained earlier both have
$\CO(d)$, using the earlier found complexities but with $n=1$ because
at this point only the query variable is left. The normalization has
$\CO(d)$. Therefore the worst case complexity for the entire algorithm
is $\CO(nd^n)$. This might seem not any better than computing the
entire probability table for the whole network, but the average case
of VE will decrease the $n$ in $d^n$ (from $\CO(nd^n)$) because it
only multiplies factors with a certain variable and hence decreasing
the complexity exponentially.

% \textbf{Correctness}:
%%%%%%%%%%%%%%% add correnctness proof

\begin{listing}[H]
  \lstinputlisting[language=python]{variable_elim_short.py}
  \caption{Condensed version of the actual Variable Elimination Algorithm.}
  \label{lst:VE}
\end{listing}


\subsubsection{Heuristic Analysis}
As already mentioned before, the three heuristic that are used to test
the efficiency are:
\begin{enumerate}[noitemsep]
\item Simple (listing \ref{lst:SH}) \\
  \textbf{Complexity}: $\CO(n)$, because it need to copy all the nodes
  into a new list, line 22, $\CO(n)$ and then remove the query node,
  line 23, $\CO(n)$.
\item Leafs First (listing \ref{lst:LFH}) \\
  \textbf{Complexity}: $\CO(n^3)$, because it loops over all the
  parents of each node, lines 24 and 27, $\CO(n^2)$ and then when
  applicable tries to remove the parent for the list of leafs, line
  30, $\CO(n)$.
\item Least Incoming Arcs (listing \ref{lst:LIAFH}) \\
  \textbf{Complexity}: $\CO(n\log n)$, because line 23,27 and 28 are
  $\CO(n)$ and line 25 is $\CO(n\log n)$.
\end{enumerate}

\begin{listing}[!h]
  \lstinputlisting[language=python, linerange={18-24}]{../src/heuristics/simple_heuristic.py}
  \caption{The code for the Simple\_Heuristic heuristic.}
  \label{lst:SH}
\end{listing}

\begin{listing}[!h]
  \lstinputlisting[language=python, linerange={18-35}]{../src/heuristics/leafs_first_heuristic.py}
  \caption{The code for the Leafs\_First\_Heuristic heuristic.}
  \label{lst:LFH}
\end{listing}

\begin{listing}[!h]
  \lstinputlisting[language=python, linerange={18-30}]{../src/heuristics/least_incoming_arcs_first_heuristic.py}
  \caption{The code for the Least\_Incoming\_Arcs\_First\_Heuristic heuristic.}
  \label{lst:LIAFH}
\end{listing}




\subsection{Testing}
% Explain how you will test and experiment on the implementations in
% order to get a good idea of 1) their correct working and 2) the
% empirical results you are interested in. How you would interpret the
% results. Oftentimes the assignment will ask you for particular
% empirical questions that you can summarize here.

% %How did you test for:
% %- tesing for correctness of the algorithms
% %- test complexity

To test the implementation of VE with different heuristics a
collection of different Bayesian networks \parencite{scutari22}. The
networks are of varying sizes and include disconnected graphs and
non-binary variables. Graphical representation of the networks used
can be found in the appendix \ref{appx:BNfigures}. To determine the efficiency the size
of the largest factor encountered during the run of the algorithm is
used. This is a good efficiency measure because VE was designed in
order to not have to compute the probability distribution of the
entire network, with size $2^n$.

Unfortunately I was unable to get the AISpace
(http://aispace.org/bayes/) and \\
(https://aispace2.github.io/AISpace2/index.html) working and could
therefore not test the correctness of the output of the algorithm.


\section{Results}
% Write up the results in a clear and easy to follow way. Use a table
% and if meaningful, a graph, to summarize results. Give the raw facts
% here, not the interpretation of the facts.

Tables \ref{tab:maxfactorWOO} and \ref{tab:maxfactorWO} contain the
results per network and heuristic. Table \ref{tab:maxfactorWOO} is the
max factors size when running VE without any observed variables. Table
\ref{tab:maxfactorWO} is the same but with one or more random observed
variables.

\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c|}
    \toprule
    file/heuristic & Simple & Leafs First & Least Incoming Arcs First \\
    \midrule
    asia           &     80 &          32 &                        80 \\
    cancer         &     32 &          32 &                        32 \\
    earthquake     &     80 &          32 &                        32 \\
    sachs          &    405 &         405 &                     17496 \\
    survey         &     48 &          48 &                       120 \\
    \bottomrule
  \end{tabular}
  \caption{The size of the largest factor encountered during VE without observed variables.}
  \label{tab:maxfactorWOO}
\end{table}

\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c|}
    \toprule
    file/heuristic & Simple & Leafs First & Least Incoming Arcs First \\
    \midrule
    asia           &     80 &          32 &                        80 \\
    cancer         &     16 &          16 &                        16 \\
    earthquake     &     80 &          16 &                        16 \\
    sachs          &    405 &         405 &                      5832 \\
    survey         &     48 &          32 &                       120 \\
    \bottomrule
  \end{tabular}
  \caption{The size of the largest factor encountered during VE with random observed variables.}
  \label{tab:maxfactorWO}
\end{table}

For easy reference table \ref{tab:networksize} shows the network size (number of nodes) per file.
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|}
    \toprule
    File & Network Size \\
    \midrule
    asia       & 8 \\
    cancer     & 5 \\
    earthquake & 5 \\
    sachs      & 11 \\
    survey     & 6 \\
    \bottomrule
  \end{tabular}
  \caption{Network size per file.}
  \label{tab:networksize}
\end{table}


\section{Discussion}
% In the discussion you interpret the results. What do you see and what does it mean

%%%%%%%%%% extend
In both table \ref{tab:maxfactorWOO} and \ref{tab:maxfactorWO} Leafs
First is the most efficient heuristic, followed by Simple and then
Least Incoming Arc. Comparing the two tables notice that the numbers
in table \ref{tab:maxfactorWO} are equal or smaller than those in
table \ref{tab:maxfactorWOO}. This is not very surprisingly since
observed variables are removed from the factors, reducing its size,
and hence resulting in smaller maximum factor sizes. Also note that
the size of the network positively influences maximum factor size in
both table \ref{tab:maxfactorWOO} and \ref{tab:maxfactorWO}.


\section{Conclusion}
% Conclude the report. Summarize the main findings, mirroring the introduction

%%%%%%%%%% extend
This paper has shown how different heuristic impact the efficiency of
the VE algorithm with multiple Bayesian networks. The heuristic tested
as most efficient is Leafs First which was closely followed by
Simple. The heuristic tested as worst is Least Incoming Arcs. Recal
that Leaf First has complexity $\CO(n^3)$ and Simple $\CO(n)$, it is
definitely worth the increase in complexity using the Leaf First
heuristic instead of the Simple heuristic given a random Bayesian
network to increase the efficiency of VE.


\section{Reflection}
% Reflect on the assignment: what obstacles did you encounter? Any
% interesting observations? What could still be improved?

One problem with the current implementation lies within the multiply
method of the Factor class. Line 51 of \verb+factor.py+ renames the
column of a pandas DataFrame. Pandas gives you the option to rename
the column of original DataFrame (the so called inplace renaming) or
to get a completely new DataFrame with the new column name, i.e. a
copy of the DataFrame with a new column name. In this case renaming
the column without a copying the DataFrame is sufficient, but pandas
will give an SettingWithCopyWarning, meaning that you changed
something in a copy of the DataFrame and therefore did not change the
actual DataFrame. So to get rid of the warning it was necessary to
create a copy of the DataFrame with the new column name. This is
definitely not desirable because it has a big impact on performance
especially when the factors get large.

Another thing that could be improved is the initialization of the
factors. The current implementation assumes all probability
distribution given in the \verb+.bif+ file can be directly taken as a
factors instead of first creating the product formula then reduce it
based on the network structure and only then identify the
factors. From the examples I have seen the identified factors were
always exactly the same as the given probability distribution and
hence choose to disregard the official steps.


\appendix
\section{Bayesian Network Figures}\label{appx:BNfigures}
% Add pictures
Figures 1-5 are the graphical representations of the Bayesian networks \parencite{scutari22}.
\begin{figure}[!h]
  \centering
  \includegraphics[scale=.35]{../fig/asia.png}
  \caption{Bayesian network of asia.bif.}
  \label{fig:BNa}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=.35]{../fig/cancer.png}
  \caption{Bayesian network of cancer.bif.}
  \label{fig:BNc}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=.35]{../fig/earthquake.png}
  \caption{Bayesian network of earthquake.bif.}
  \label{fig:BNeq}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=.35]{../fig/sachs.png}
  \caption{Bayesian network of sachs.bif.}
  \label{fig:BNsachs}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=.3]{../fig/survey.png}
  \caption{Bayesian network of survey.bif.}
  \label{fig:BNsurvey}
\end{figure}

\printbibliography

\end{document}
