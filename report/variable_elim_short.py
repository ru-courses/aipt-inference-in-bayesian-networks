def run(self, query: str, observed: dict, elim_order: list) -> pd.DataFrame:
    # Identify factors and reduce observed variables
    factors = []
    for v, p in self.network.probabilities.items():
        factors.append(Factor(p))
        factors[-1].reduce(observed)

    # For every variable Z in elim_order:
    for var in elim_order:
        remaining_factors, \
        factors_to_multiply = self.__split_factors(var, factors)

        if factors_to_multiply:
            new_factor = factors_to_multiply[0]
            if len(factors_to_multiply) > 1:
                # Multiply factors containing Z
                new_factor = self.__multiply_factors(factors_to_multiply)
            if new_factor.probabilities.columns.size > 2:
                # sum out Z to optain f_z
                new_factor.sum_out_var(var)

                # Add f_z to remaining factors
                remaining_factors.append(new_factor)
            factors = remaining_factors

    # return the normalized result.
    res = self.__multiply_factors(factors)
    res.sum_out_var()
    res.normalize()
    return res.probabilities
